﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using UAS_20170140141_RizkiFajar.Models;
using Microsoft.AspNetCore.Authorization;

namespace UAS_20170140141_RizkiFajar.Controllers
{
    public class ReservasiController : Controller
    {
        private readonly UASPAWContext _context;

        public ReservasiController(UASPAWContext context)
        {
            _context = context;
        }

        // GET: Reservasi
        [Authorize(Policy = "readonlypolicy")]
        public async Task<IActionResult> Index(string ktsd, string cari, string sortOrder, string currentFilter, int? pageNumber)
        {
            //buat list untuk menyimpan ketersediaan
            var ktsdList = new List<string>();

            //query 
            var ktsdQuery = from d in _context.Reservasi orderby d.IdKamarNavigation.TypeKamar select d.IdKamarNavigation.TypeKamar;

            //simpan hasil query kedalam list
            ktsdList.AddRange(ktsdQuery.Distinct());

            //simpan list di dropdown
            ViewBag.ktsd = new SelectList(ktsdList);

            //untuk mengambil data ditampilkan di table
            var Menu = from m in _context.Reservasi.Include(k => k.IdKamarNavigation) select m;

            //Percabangan list tersedia dalam dropdown
            if (!string.IsNullOrEmpty(ktsd))
            {
                Menu = Menu.Where(x => x.IdKamarNavigation.TypeKamar == ktsd);
            }

            //fitur search
            if (!string.IsNullOrEmpty(cari))
            {
                Menu = Menu.Where(x => x.NamaCustomer.Contains(cari));

            }
            //membuat logic untuk sort
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";

            switch (sortOrder)
            {
                case "name_desc":
                    Menu = Menu.OrderByDescending(s => s.NamaCustomer);
                    break;
                case "Date":
                    Menu = Menu.OrderBy(s => s.IdKamarNavigation.TypeKamar);
                    break;
                case "date_desc":
                    Menu = Menu.OrderByDescending(s => s.IdKamarNavigation.TypeKamar);
                    break;
                default: //name ascending
                    Menu = Menu.OrderBy(s => s.NamaCustomer);
                    break;
            }
            //membuat paged list
            ViewData["CurrentSort"] = sortOrder;

            if (cari != null)
            {
                pageNumber = 1;
            }
            else
            {
                cari = currentFilter;
            }
            ViewData["CurrentFilter"] = cari;


            //var uASPAWContext = _context.Reservasi.Include(r => r.IdKamar1Navigation).Include(r => r.IdKamarNavigation).Include(r => r.IdMakananNavigation).Include(r => r.IdRoomserviceNavigation);
            //return View(await uASPAWContext.ToListAsync());
            int pageSize = 4;
            return View(await PaginatedList<Reservasi>.CreateAsync(Menu.AsNoTracking(), pageNumber ?? 1, pageSize));
            //var uASPAWContext = _context.Reservasi.Include(r => r.IdKamar1Navigation).Include(r => r.IdKamarNavigation).Include(r => r.IdMakananNavigation).Include(r => r.IdRoomserviceNavigation);
            //return View(await uASPAWContext.ToListAsync());
        }

        // GET: Reservasi/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reservasi = await _context.Reservasi
                .Include(r => r.IdKamar1Navigation)
                .Include(r => r.IdKamarNavigation)
                .Include(r => r.IdMakananNavigation)
                .Include(r => r.IdRoomserviceNavigation)
                .FirstOrDefaultAsync(m => m.IdReservasi == id);
            if (reservasi == null)
            {
                return NotFound();
            }

            return View(reservasi);
        }

        // GET: Reservasi/Create
        [Authorize(Policy = "writepolicy")]
        public IActionResult Create()
        {
            ViewData["IdKamar1"] = new SelectList(_context.Kamar, "IdKamar", "IdKamar");
            ViewData["IdKamar"] = new SelectList(_context.Kamar, "IdKamar", "TypeKamar");
            ViewData["IdMakanan"] = new SelectList(_context.Makanan, "IdMakanan", "PaketMakanan");
            ViewData["IdRoomservice"] = new SelectList(_context.RoomService, "IdRoomservice", "PaketService");
            return View();
        }

        // POST: Reservasi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdReservasi,NamaCustomer,Alamat,NoHp,TanggalLahir,JenisKelamin,JumlahTamu,TglCekin,TglCekout,Haripermalam,IdKamar1,IdKamar,Lantai,BiayaKamar,IdMakanan,BanyakMakanan,HargaMakanan,IdRoomservice,BanyakHari,HargaService,Total,CreditCard,NoCc,Status")] Reservasi reservasi)
        {
            if (ModelState.IsValid)
            {
                _context.Add(reservasi);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdKamar1"] = new SelectList(_context.Kamar, "IdKamar", "IdKamar", reservasi.IdKamar1);
            ViewData["IdKamar"] = new SelectList(_context.Kamar, "IdKamar", "IdKamar", reservasi.IdKamar);
            ViewData["IdMakanan"] = new SelectList(_context.Makanan, "IdMakanan", "IdMakanan", reservasi.IdMakanan);
            ViewData["IdRoomservice"] = new SelectList(_context.RoomService, "IdRoomservice", "IdRoomservice", reservasi.IdRoomservice);
            return View(reservasi);
        }

        // GET: Reservasi/Edit/5
        [Authorize(Policy = "editpolicy")]

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reservasi = await _context.Reservasi.FindAsync(id);
            if (reservasi == null)
            {
                return NotFound();
            }
            ViewData["IdKamar1"] = new SelectList(_context.Kamar, "IdKamar", "IdKamar", reservasi.IdKamar1);
            ViewData["IdKamar"] = new SelectList(_context.Kamar, "IdKamar", "IdKamar", reservasi.IdKamar);
            ViewData["IdMakanan"] = new SelectList(_context.Makanan, "IdMakanan", "IdMakanan", reservasi.IdMakanan);
            ViewData["IdRoomservice"] = new SelectList(_context.RoomService, "IdRoomservice", "IdRoomservice", reservasi.IdRoomservice);
            return View(reservasi);
        }

        // POST: Reservasi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdReservasi,NamaCustomer,Alamat,NoHp,TanggalLahir,JenisKelamin,JumlahTamu,TglCekin,TglCekout,Haripermalam,IdKamar1,IdKamar,Lantai,BiayaKamar,IdMakanan,BanyakMakanan,HargaMakanan,IdRoomservice,BanyakHari,HargaService,Total,CreditCard,NoCc,Status")] Reservasi reservasi)
        {
            if (id != reservasi.IdReservasi)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(reservasi);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReservasiExists(reservasi.IdReservasi))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdKamar1"] = new SelectList(_context.Kamar, "IdKamar", "IdKamar", reservasi.IdKamar1);
            ViewData["IdKamar"] = new SelectList(_context.Kamar, "IdKamar", "IdKamar", reservasi.IdKamar);
            ViewData["IdMakanan"] = new SelectList(_context.Makanan, "IdMakanan", "IdMakanan", reservasi.IdMakanan);
            ViewData["IdRoomservice"] = new SelectList(_context.RoomService, "IdRoomservice", "IdRoomservice", reservasi.IdRoomservice);
            return View(reservasi);
        }

        // GET: Reservasi/Delete/5
        [Authorize(Policy = "deletepolicy")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reservasi = await _context.Reservasi
                .Include(r => r.IdKamar1Navigation)
                .Include(r => r.IdKamarNavigation)
                .Include(r => r.IdMakananNavigation)
                .Include(r => r.IdRoomserviceNavigation)
                .FirstOrDefaultAsync(m => m.IdReservasi == id);
            if (reservasi == null)
            {
                return NotFound();
            }

            return View(reservasi);
        }

        // POST: Reservasi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var reservasi = await _context.Reservasi.FindAsync(id);
            _context.Reservasi.Remove(reservasi);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ReservasiExists(int id)
        {
            return _context.Reservasi.Any(e => e.IdReservasi == id);
        }
    }
}
