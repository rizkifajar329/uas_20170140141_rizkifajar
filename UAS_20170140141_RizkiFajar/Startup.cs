﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UAS_20170140141_RizkiFajar.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using UAS_20170140141_RizkiFajar.Models;
using System.IO;
using Microsoft.Extensions.FileProviders;

namespace UAS_20170140141_RizkiFajar
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //untuk upload file
            services.AddSingleton<IFileProvider>(
              new PhysicalFileProvider(
                  Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Upload")));
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<UASPAWContext>(options =>options.UseSqlServer(Configuration.GetConnectionString("Default")));
            //untuk loginbiasa tidak menggunakan role
            //services.AddDefaultIdentity<IdentityUser>().AddEntityFrameworkStores<UASPAWContext>();
            services.AddIdentity<IdentityUser, IdentityRole>().AddDefaultUI()
              .AddEntityFrameworkStores<UASPAWContext>().AddDefaultTokenProviders();///digunakan untuk role 
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddAuthorization(options =>
            {
                options.AddPolicy("readonlypolicy",
                    builder => builder.RequireRole("Admin", "Resepsionis", "Cleaning Service", "Kitchen"));
                options.AddPolicy("writepolicy",
                    builder => builder.RequireRole("Admin", "Resepsionis", "Cleaning Service", "Kitchen"));
                options.AddPolicy("editpolicy",
                    builder => builder.RequireRole("Admin", "Resepsionis", "Cleaning Service", "Kitchen"));
                options.AddPolicy("deletepolicy",
                builder => builder.RequireRole("Admin", "Resepsionis", "Cleaning Service", "Kitchen"));
            });

            services.AddScoped<Kamar>();
            services.AddScoped<Makanan>();
            services.AddScoped<Reservasi>();
            services.AddScoped<RoomService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
